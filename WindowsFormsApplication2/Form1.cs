﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        ///<summary>
        ///Первое задание
        ///</summary>
        public static BigInteger Recurse(BigInteger bb)
        {
            if (bb == 0)
                return 1;
            else
                return bb * Recurse(bb - 1);

        }

        public void RecurseBig()
        {
            int s = Convert.ToInt32(textBox1.Text);
            if (s >= 0)
            {
                BigInteger bb = new BigInteger(s);
                label1.Text = Convert.ToString(Recurse(bb));
            }
            else
            {
                MessageBox.Show("Введите неотрицательное значение");
                textBox1.Clear();
                label1.Text = "";
            }
        }

        /// <summary>
        /// Второе задание
        /// </summary>
        public static double Function(double x)
        {
            double A;
            A = Math.Sqrt(Math.Log(4 / x)) - 1 / x - Math.Pow(Math.E, Math.Sin(x));
            return A;
        }

        public void FunctionS()
        {
            double x = Convert.ToDouble(textBox2.Text);
            if (x != 0 && Math.Log(4 / x) > 0)
            {
                label5.Text = Convert.ToString(Function(x));
            }
            else
            {
                MessageBox.Show("Данная точка лежит вне области допустимых значений");
                textBox2.Clear();
                label5.Text = "";
            }
            
        }

        /// <summary>
        /// Третье задание (Фибоначчи)
        /// </summary>
        
        public void Fibonacci()
        {
            int j = 1;
            int end = Convert.ToInt32(textBox4.Text);
            for (int i = 1; i <= end; i += j)
            {
                listBox1.Items.Add(i);   
                j = i - j;
            }
        }

        /// <summary>
        /// Четвертое задание
        /// </summary>
        static double Sin(double x)
        {
            const int iterations = 10;
            var res = 0d;
            var pow = x;
            var sign = 1;

            for (int i = 1; i < iterations; ++i)
            {
                if (i % 2 == 1)
                {
                    res += pow * sign;
                    sign *= -1;
                }
                pow *= x / (i + 1);
            }
            return res;
        }
        public void SinTeylor()
        {
            double a = Convert.ToDouble(textBox3.Text);//градус в углах
            int s = Convert.ToInt32(textBox5.Text);//точность знаков
            double x = a * Math.PI / 180; //градусы -> радианы
            double sin = Sin(x);
            sin = Math.Round(sin, s);
            label12.Text = Convert.ToString(a);
            label13.Text = Convert.ToString(sin);
            label14.Text = "=";
        }
        private void button1_Click(object sender, EventArgs e)
        {
            RecurseBig();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            FunctionS();
        }
        private void button3_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            Fibonacci();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            SinTeylor();
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Bitmap image1 = new Bitmap(@"imageB.png"); 
            pictureBox1.Image = image1;
            label1.Text = "";
            label5.Text = "";
            label12.Text = "";
            label13.Text = "";
            label14.Text = "";
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
